// // TO DO
// - Array methods
// - integrate promises and asynch/await
// - fix execution time log
// - failed download files must be logged
// - extReplacer FX must replace JSON with an argument for in and out, plus replace regex with index substring
// - checkPDF must be used as extChecker
// - convert mediaBucket in an array of objects
// - fix issue counter with object length
// - split in multiple modules grouped by process state and purpose
// - improve write file

// const fs = require('fs')
const http = require('follow-redirects').http
const https = require('follow-redirects').https
const fs = require('fs-extra')

const colors = require('colors')
const jsdom = require('jsdom')
const { JSDOM } = jsdom
const json2csv = require('json2csv')
const csvtojson = require('csvtojson')

const dist = './dist/'
const src = './src/'
const distMedia = dist+'media/'
// const mediaPrefix = '//s3-eu-central-1.amazonaws.com/firma-cms-uploads-development/wp-content/uploads/' // dev
// const mediaPrefix = '//s3-eu-central-1.amazonaws.com/firma-cms-uploads-production/wp-content/uploads/' // prod
const mediaPrefix = '//d23wdxoo51zvyp.cloudfront.net/wp-content/uploads/' // prod
// const mediaPrefix = '/wp-content/uploads/'

let mediaBucket = {
                    filename: [],
                    url: []
                  }

let authors = {data:[]}

let rating
const ratingDB = 'ratingDB.json'

let slug_taxonomiesDB = 'slug_taxonomiesDB.csv'

let titleOver80 = 0
let titleOver90 = 0
let titleOver100 = 0
let articleHidden = 0
let articleAttributes = 0
let articleDiv = 0
let articleImages = 0
let articleImagesCopied = []
let articleImagesSmall = 0
let articleMSWord = 0
let articleTables = 0
let articleRealTables = 0
let articleContentbox = 0
let articleH2 = 0
let articleQuote = 0
let articleQuoteArray = []
let articleContentboxTitle = 0
let articleAttachment = 0
let articleLink = 0

const resetCounters = () =>{
  titleOver80 = 0
  titleOver90 = 0
  titleOver100 = 0
  articleHidden = 0
  articleAttributes = 0
  articleDiv = 0
  articleImages = 0
  articleImagesCopied = []
  articleImagesSmall = 0
  articleMSWord = 0
  articleTables = 0
  articleRealTables = 0
  articleContentbox = 0
  articleH2 = 0
  articleQuote = 0
  articleQuoteArray = []
  articleContentboxTitle = 0
  articleAttachment = 0
  articleLink = 0
}

const unusedKeys = [
  // "uid",
  'pid',
  'tstamp',
  'crdate',
  'cruser_id',
  't3ver_oid',
  't3ver_id',
  't3ver_wsid',
  't3ver_label',
  't3ver_state',
  't3ver_stage',
  't3ver_count',
  't3ver_tstamp',
  't3ver_move_id',
  't3_origuid',
  'editlock',
  'sys_language_uid',
  'l10n_parent',
  'l10n_diffsource',
  'deleted',
  // 'hidden',
  'starttime',
  'endtime',
  'sorting',
  'fe_group',
  // 'title',
  // 'teaser',
  // 'bodytext',
  // 'datetime',
  'archive',
  // 'author',
  'author_email',
  'categories',
  'related',
  'related_from',
  'related_files',
  'fal_related_files',
  'related_links',
  'type',
  'keywords',
  'description',
  'tags',
  'media',
  'fal_media',
  'internalurl',
  'externalurl',
  'istopnews',
  'content_elements',
  'path_segment',
  'alternative_title',
  'zzz_deleted_rte_disabled',
  'import_id',
  'import_source',
  'tx_fieldsfornews_consultant',
  // 'tx_fieldsfornews_title',
  // 'tx_fieldsfornews_description'
]

const newKeys = [
  'slug',
  'category',
  'tag',
  'rating',
  'vote_count',
  'post_type'
]

const unusedKeysRating = [
  'uid',
  'pid',
  'tstamp',
  'crdate',
  'cruser_id',
  // 'reference',
  // 'rating',
  // 'vote_count',
  'reference_scope',
  'isreview'
]

const attributes = [
  'style',
  'width',
  'height',
  'valign',
  'cellspacing',
  'cellpadding',
  'rowspan',
  'border',
  'frame',
  'bgcolor',
  'align',
  'boarder',
  'scope',
  'data-htmlarea-file-uid',
  'data-htmlarea-file-table',
  'data-htmlarea-clickenlarge',
  'class'
]

const fieldsCSV = 
[
  "uid",
  'title',
  'teaser',
  // 'bodytext',
  // 'datetime',
  // 'author',
  // 'rating',
  // 'vote_count',
  'hidden',
  'category',
  'tag',
  'slug',
  // 'tx_fieldsfornews_title',
  // 'tx_fieldsfornews_description'
]

const fieldsWP = 
[
  "uid",
  'title',
  'teaser',
  'bodytext',
  'datetime',
  'author',
  'rating',
  'vote_count',
  'hidden',
  'category',
  'tag',
  'slug',
  'tx_fieldsfornews_title',
  'tx_fieldsfornews_description',
  'post_type'
]

const field_namesWP = 
[
  "post_id",
  'post_title',
  'post_excerpt',
  'post_content',
  'post_date',
  'post_author',
  'post_rating_total',
  'post_rating_votes',
  'post_status',
  'post_category',
  'post_tags',
  'post_name',
  '_yoast_wpseo_title',
  '_yoast_wpseo_metadesc',
  'post_type'
]

const authorCSV =
[
  'uid',
  'link',
  'img',
  'desc',
]


Array.prototype.diff = function (a) {
  return this.filter(function (i) {
    return a.indexOf(i) === -1
  })
}

// Escape issues
const replaceIssues = (issues) =>{
  let chars = `!"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_abcdefghijklmnopqrstuvwxyz{|}~€‚ƒ„…†‡ˆ‰Š‹ŒŽ‘’“”•–—˜™š›œžŸ¡¢£¤¥¦§¨©ª«¬®¯°±²³´µ¶·¸¹º»¼½¾¿ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖ×ØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõö÷øùúûüýþÿĀāĂăĄąĆćĈĉĊċČčĎďĐđĒēĔĕĖėĘęĚěĜĝĞğĠġĢģĤĥĦħĨĩĪīĬĭĮįİıĲĳĴĵĶķĸĹĺĻļĽľĿŀŁłŃńŅņŇňŉŊŋŌōŎŏŐőŒœŔŕŖŗŘřŚśŜŝŞşŠšŢţŤťŦŧŨũŪūŬŭŮůŰűŲųŴŵŶŷŸŹźŻżŽž`
  let charsOutput, charsRemoved, ntext

  const removalScript = (string) => {
    return string.replace(new RegExp(/(\\&quot\;|&quot\;)/g), '') // quotes
                  .replace(new RegExp(/(&gt;)/g), '\>') // remove empty paragraphs
                  .replace(new RegExp(/(&lt;)/g), '\<') // remove empty paragraphs
                  .replace(new RegExp(/(\r\n?|\n|\t)/g), '') // line breaks
                  // .replace(new RegExp(/[^\x20-\x7E]+/g), '') // non printable characters
                  .replace(new RegExp(/(&gt\;)|(>>)/g), '') // >>
                  .replace(new RegExp(/(\\\\")/g), '\\"') // \"
                  .replace(new RegExp(/(<div)/g), '<p') // div into paragraph, without close in case of attributes
                  .replace(new RegExp(/(<\/div>)/g), '</p>') // div into paragraph
                  .replace(new RegExp(/(&nbsp\;|<p>&nbsp\;<\/p>)/g), ' ') // spaces
                  // .replace(new RegExp(/(fileadmin\/images\/|fileadmin\/user_upload\/)/g), '/wp-content/uploads/') // attachment
                  .replace(new RegExp(/(\\\"\/http)/g), 'http') // pdf issues
                  .replace(new RegExp(/(<\/?article>|<\/?span>)/g), '') // articles? no thanks
                  .replace(new RegExp(/(<p><\/p>)/g), '') // remove empty paragraphs
  }


  ntext = removalScript(issues)

  charsOutput = removalScript(chars)
  charsRemoved = Array.from(chars).diff(Array.from(charsOutput))
  charsRemoved = charsRemoved.join('')

  // console.log(colors.yellow('# Characters analyzed:', chars))
  console.log(colors.yellow('# Characters count:', charsOutput.length, 'of', chars.length))
  console.log(colors.yellow('# Characters removed:', charsRemoved))
  return ntext
}


// Remove document type
const replaceDocType = docType =>{
  let serialized = docType.replace(new RegExp(/(<\/?html>|<\/?head>|<\/?body>)/g), '')
  return serialized
}


// fix Wordpress naming security convention
const wordpressNamingFix = link => {
  if ((link.indexOf('&') !== -1) || (link.indexOf('\.de\.') !== -1) || (link.substr(0, 1) == '_')) {
    // console.log(link)
    link = link.replace(new RegExp(/(&)/g), '')
            .replace(new RegExp(/(\.de\.)/g), '\.de_\.')
            .replace(new RegExp(/^(_)/g), '')
    // console.log(link)
  }

  return link
}

// Convert date from Unix to JSON
const dateUnixConversion = unixTimestamp =>{
  let myDate = new Date( unixTimestamp *1000)
  // let dateJSON = myDate.toGMTString()
  let dateJSON = myDate.toLocaleString()
  return dateJSON
}


// Remove MSWord typo
const removeMSWord = body =>{
  let dom = new JSDOM(body)
  let articleParagraphs = dom.window.document.body.querySelectorAll('p')
  // console.log(colors.cyan('# Paragraphs found: ' + articleParagraphs.length))

  for (let paragraph of articleParagraphs) {
    if (paragraph.outerHTML.indexOf('<!-- [if gte') >= 0) {
      articleMSWord++
      paragraph.remove()
    }
  }

  return dom.serialize(body)
}


const returnEndSubstring = (string, character) => {
  if (string && character) {
    return string.substring(string.lastIndexOf(character) + 1)
  } else {
    console.log('Please insert arguments'.red)
  }
}

// Extension inspector
const extChecker = (link, extension) => {
  if (link && extension) {
    return returnEndSubstring(link, '.') === extension
  } else {
    console.log('Please insert link/file or extension'.red)
  }
}

// Extension removal
const extReplacer = file => {
  if (file) {
    return file.replace(new RegExp(/(\.json)/g), '')
  } else {
    console.log('Please define the file'.red)
  }
}

// PDF inspector
const checkPDF = link => {
  if (link) {
    return extChecker(link.href, 'pdf')
  }
}

const jsonConcat = (obj1, obj2) => {
  for (let key in obj2) {
    obj1[key] = obj2[key];
  }
}

function matchUID(db1_obj){
  if (db1_obj != undefined) {
    for (let i = 1; i < arguments.length; i++) {

      if (arguments[i] != undefined) {

        for (let dbMatch_obj of arguments[i].data) {
          if (db1_obj.uid == dbMatch_obj.uid) {
            jsonConcat(db1_obj, dbMatch_obj)
          }
        }

      } else {

        console.log('# '+arguments[i]+' has no data to be added'.red)
        break
      }
    }
    return db1_obj
  } else {
    console.log('# Please choose what to merge'.red)
  }
}


// Remove everything about the table
const stripTable = (table, quote) => {
  if (quote) {
    addQuote(table)
  } else {
    addContentbox(table)
  }
}

const tableExtractor = table => {
  content = []

  if(table) {
    for  (let row of table.rows) {
      for  (let cells of row.cells) {
        // console.log(colors.white(cells.innerHTML))
        if (cells.innerHTML.trim() === '') {
        } else {
          cells.innerHTML = cells.innerHTML.replace(new RegExp(/(<\/?table>|<\/?td>|<\/?th>|<\/?tr>|<\/?thead>|<\/?tbody>|<\/?tfoot>)/g), '')
          content.push(cells.innerHTML)
        }
      }
    }
    return content.join("")
  } else {
    console.log('# There is no table'.red)
  }
}

// Wrap into a blockquote tag
const addQuote = table => {
  table.outerHTML = '<blockquote>' + table.rows[0].cells[0].textContent + '</blockquote>' 
}

// Turn into a contentbox
const addContentbox = table => {
  content = tableExtractor(table)

  box = '<div class="article__content__span">' + content + '</div>'
  table.outerHTML = box
}


// Remove attributes
const removeAttributes = body => {
  let dom = new JSDOM(body)
  let articleTags = dom.window.document.body.querySelectorAll('*')
  // console.log(colors.cyan('# Tags found: ' + articleTags.length))

  for (let tag of articleTags) {
    for (let attribute of attributes) {
      let tagHasAttribute = tag.hasAttribute(attribute)
      if (tagHasAttribute) {
        articleAttributes++
        tag.removeAttribute(attribute)
      }
    }
  }

  return replaceDocType(dom.serialize(body))
}

const updateLink = link => {
  if (link.slice(-1) === '/'){
    link = link.slice(0, - 1)
  }
  
  link = returnEndSubstring(link, '/')

  if (slug_taxonomies.data.lenght !== 0) {
    // console.log(slug_taxonomies.data)

    for (article of slug_taxonomies.data) {

      if (article.slug == link){
        link = '/' + article.category + '/' + article.slug + '/'
        return link
      }
    }
  } else {
    console.log('# You dont have any slug database'.red)
  }
}


// Filter links, mostly for PDF
const filterLinks = body => {
  let dom = new JSDOM(body)
  let links = dom.window.document.body.querySelectorAll('a')

  for (let link of links) {

    link.href = link.href.replace(new RegExp(/(\#)/g), '') // suffix for no reasons

    // fix file: issue
    if (link.href.indexOf('file\:') !== -1) {
      link.href = link.href.replace(new RegExp(/(file\:\/\/)/g), '') // file: verify why 3 slahes...
    }


    // check all PDF attachments, update the link, copy file and store value if missing
    if (link.href && extChecker(link.href, 'pdf')) {
      // console.log(link.href)
      let PDF = returnEndSubstring(link.href, '/')

      PDF = wordpressNamingFix(PDF)

      if (link.href.indexOf('http') < 0 ) {
        if (mediaBucket.filename.indexOf(PDF) < 0) {
          mediaBucket.filename.push(PDF)
          mediaBucket.url.push(link.href)
        }

        // fix PDF path
        link.href = mediaPrefix + PDF
        // console.log(link.href)
      }
    }

    // fix firma.de absolute links
    if (link.href.indexOf('firma.de') !== -1) {
      if (updateLink(link.href) != undefined) {
        link.href = updateLink(link.href)
      }
    }
  }

  return dom.serialize(body)
}


// Filter images
const filterImage = body => {
  let dom = new JSDOM(body)
  let images = dom.window.document.body.querySelectorAll('img')
  // console.log(colors.cyan('# Image found: ' + images.length))

  for (let image of images) {
    articleImages++
    // console.log(image.src)
    let src = returnEndSubstring(image.src, '/')
    
    src = src.replace(new RegExp(/(\#)/g), '') // suffix for no reasons

    src = wordpressNamingFix(src)

    // store image name to be copied
    if (mediaBucket.filename.indexOf(src) < 0) {
      mediaBucket.filename.push(src)
      mediaBucket.url.push(image.src)
    }

    // fix image path
    image.src = mediaPrefix + src
    // console.log(image.src)
  }

  return dom.serialize(body)
}


const storeAuthor = (table, uid, slug) => {
  let img 
  let desc = tableExtractor(table)
  let link = 'https://www.firma.de/ratgeber/' + slug

  if (table.querySelector('img')) {
    img = table.querySelector('img').src
    img = returnEndSubstring(img, '/')
  }

  authors.data.push({uid, link, img, desc})
}


// Identify all the issues
const tablesIssuesFinder = issues => {
  // Inspect tables
  // console.log("# Inspecting tables...".cyan);
  let dom = new JSDOM(issues.bodytext)
  let tables = dom.window.document.body.querySelectorAll("table");
  // console.log(colors.yellow('# Tables found: ' + tables.length))
  for (let table of tables) {
    if ( table ) {
      articleTables++
      let rows = table.rows;

      if ( rows ) {

        let hasHead = table.tHead
        let hasBody = table.tBodies
        let hasH2 = table.querySelector('h2')
        let hasLink = table.querySelector('a')
        let hasImg = table.querySelector('img')
        let hasDiv = table.querySelector('div')
        let isAttachment = checkPDF(hasLink)

        if ( hasHead ) {
          // console.log("has thead".green)
        }
        if ( hasBody ) {
          // console.log("has tbody".green)
        }
        if ( hasDiv ) {
          articleDiv++
          // console.log("has div inside omg".green)
          // console.log(colors.white(hasDiv.outerHTML))
        }
        if ( hasH2 ){
          articleH2++
          // console.log(colors.white(hasH2.outerHTML))
        }

        if ((rows.length > 1) && !(hasBody && hasHead) &&  !hasImg) {
          articleRealTables++
          // console.log("it's just a table (multiple rows)".red)
          // console.log(colors.white(table.outerHTML))
        
        } else if (isAttachment) {
          articleAttachment++
          stripTable(table)
          // console.log("is an Attachment".red)

        } else if (hasBody && hasH2 && !hasHead) {
          articleQuote++
          stripTable(table, true)
          articleQuoteArray.push(issues.uid)
          // console.log("is a Quote".red)
        
        } else if (hasH2 && hasHead && !hasImg) {
          articleContentboxTitle++
          stripTable(table)
          // console.log("is a Contentbox with Title".red)
          // console.log(colors.white(hasH2.outerHTML))

        } else if ( hasImg ) {
          storeAuthor(table, issues.uid, issues.slug)
          table.remove()
          // console.log("it's an author!".red)

        } else {
          for (let row of rows) {
            let cells = row.cells
            // console.log(colors.yellow('# Cells found: ' + cells.length))

            if ( (cells.length > 1) && !hasImg ) { 
              articleRealTables++
              // console.log("it's just a table (multiple cells)".red)
              break;

            } else {
              for (let cell of cells) {
                let hasLink = cell.querySelector('a')
                articleContentbox++
                stripTable(table)
                // console.log("isContentbox".red)

                if ( hasLink ) {
                  articleLink++
                  // console.log("it has a link".red)
                
                }
              }
            }
          }
        }
      }
    }

    // console.log(colors.white(table.outerHTML))
    // console.log('-----')
  }

  return replaceDocType(dom.serialize(issues))
}


// extract post ID for star rating field
const extractRatingID = value =>{
  let ratingID = value.replace(new RegExp(/tx_news_domain_model_news_|tt_news_/g), '')
  return ratingID
}


// Read rating DB
const mergeRating = file => {
  const data = fs.readFileSync(src+ 'merge/' + file).toString();

  const objRatingRaw = extractRatingID(data)
  // console.log(colors.cyan('# Reference trimmed'))

  const objRating = JSON.parse(objRatingRaw)

  for (let article of objRating.data) {
    // Remove unnecessary keys
    for (let key of unusedKeysRating) {
      if(key === 'reference'){
        article.uid = JSON.parse(article.reference)
      } else if(key === 'uid'){
        article.uid = JSON.parse(article.reference)
      } else {
        delete article[key]
      }
    }
    delete article.reference
  }

  // console.log(colors.cyan('# Unused keys removed, uid updated'))
  // console.log(objRating)

  return objRating
}


// const downloadFile = (url, path) => new Promise((resolve, reject) => {
//   http.get(url, response => {
//     // console.log(url)
//     const statusCode = response.statusCode

//     if (statusCode !== 200) {
//       // console.log(statusCode)
//       // return reject('Download error!')
//     }

//     const writeStream = fs.createWriteStream(path)
//     response.pipe(writeStream)

//     // writeStream.on('error', () => reject('Error writing to file!'))
//     writeStream.on('error', () => console.log(statusCode))
//     writeStream.on('finish', () => writeStream.close(resolve))
//   })
// }).catch(err => console.error(err))


const downloadFile = (url, filename, path) => {
  const file = fs.createWriteStream(path+filename)
  https.get(url, response => {
    const statusCode = response.statusCode

    if (statusCode !== 200) {
      console.log(url)
    } else {
      response.pipe(file)
    }
  })
}


const copyMedia = () => {
  console.log('-------------------------------------------------------')
  console.log('# Start searching for'.cyan, mediaBucket.filename.length,'media files'.cyan)

  if (!fs.existsSync(distMedia)) {
    fs.mkdir(distMedia, (err) => {
      if (err) throw err
    })
  }

  fs.readdir(src+'media/', (err, files) => {
    if (err) throw error

    if (files != 0) {

      console.log('# Analyzing'.cyan, files.length, 'files'.cyan)

      let missingMedia = {
                            filename: [],
                            url: []
                          }

      console.log(colors.cyan('# Copying required files...'))
      for (const file of files) {
        for (let media of mediaBucket.filename) {
          if (file == media ){
            articleImagesCopied.push(file)

            fs.copy(src+'media/'+file, distMedia+file, { replace: true }, function (err) {
              if (err) {
                throw err
              }
            })
          }
        }
      }

      console.log(colors.cyan('# Generating missing files list...'))
      for (let i = 0; i < mediaBucket.filename.length; i++) {
        if (articleImagesCopied.indexOf(mediaBucket.filename[i]) < 0) {
          missingMedia.filename.push(mediaBucket.filename[i])

          if (mediaBucket.url[i].indexOf('http') == 0) {
            // mediaBucket.url[i] = mediaBucket.url[i].replace(new RegExp(/https/g), 'http')
            missingMedia.url.push(mediaBucket.url[i])
          } else {
            missingMedia.url.push('https://firma.de/'+ mediaBucket.url[i])
          }
        }
      }

      console.log('# Copied'.yellow, articleImagesCopied.length, 'media files of'.yellow, mediaBucket.filename.length)
      
      if (missingMedia.filename.length > 0) {
        console.log(colors.red('# Media files missing:'), missingMedia.filename.length)
        // writeDBFile(dist + 'missingMedia'+'.json', missingMedia)

        console.log(colors.cyan('# Downloading missing files...'))

        for (let i = 0; i < missingMedia.url.length; i++) {
          downloadFile(missingMedia.url[i], missingMedia.filename[i], distMedia)
        }

        console.log(colors.red('# Failed to download:'), missingMedia.filename.length)
        writeDBFile(dist + 'missingMediaDownload'+'.json', missingMedia.url)
      }

    } else {
      console.log('# There are no media files to inspect'.red)
    }
    console.log('-------------------------------------------------------')
  })
}


const csvToObj = (file, save) =>{
  let obj = {"data":[]}

  if(file){

    csvtojson()
      .fromFile(src + 'merge/' + file)
      .on('json',(jsonObj)=>{
        jsonObj.slug = jsonObj.slug.replace(new RegExp(/(\/)/g), '')
        obj.data.push(jsonObj)
      })
      .on('done',(error)=>{
        for (let objArray of obj.data) {
          delete objArray.title
        }

        // export taxonomy JSON db by default
        if(!save) {
          fs.writeFile(dist+file+'.json', JSON.stringify(obj, null, 2), (err) => {
            if (err) throw err
            console.log(colors.cyan('# Saved file: '), dist+file+'.json')
          })
        }
      })

    return obj

  } else {
    console.log('# Merge file not found'.red)
  }
}


const filterArticles = obj => {
  let objNew = {data:[]}
  let noCat = []
  for (article of obj.data) {
    if (!article.hidden == 1 || article.category != '') {
      // objNew.data.push(article)
    } else {
      console.log(article.category)
      noCat.push(article.uid)
    }
      objNew.data.push(article)
  }

  console.log('-------------------------------------------------------')
  console.log('# Articles removed:'.yellow, (obj.data.length - objNew.data.length))
  if (noCat.length > 0) {
    console.log(`# Articles UID without category: ${noCat}`.red)
  }
  console.log('# Articles after filtering:'.yellow, objNew.data.length)
  return objNew
}


// Write file and print stats
const writeDBFile = (file, object, stats, fieldsCSV, namesCSV) =>{

  if (stats) {
    console.log("--------------------- Summary -------------------------")
    console.log('# Articles found:'.yellow, object.data.length)
    console.log('# Hidden articles removed:'.yellow, articleHidden)
    console.log('# Attributes removed:'.yellow, articleAttributes)
    console.log('# Div tags removed:'.yellow, articleDiv)
    console.log('# Images filtered:'.yellow, articleImages, '- small:'.yellow,articleImagesSmall)
    console.log('# Media to be copied filtered:'.yellow, mediaBucket.filename.length)
    console.log('# MS Word issues removed:'.yellow, articleMSWord)
    // console.log('# Titles over 80 chars:'.yellow, titleOver80)
    // console.log('# Titles over 90 chars:'.yellow, titleOver90)
    // console.log('# Titles over 100 chars:'.yellow, titleOver100)
    console.log('# Issues found:'.yellow, articleTables)

    // Count all the issues
    let totalIssues = [articleRealTables, articleQuote, articleContentbox, articleContentboxTitle, articleAttachment, authors.lenght ]
    function getSum(total, num) {
      return total + num;
    }
    let totalIssuesValue = totalIssues.reduce(getSum)
    console.log('# Issues identified below:'.yellow, totalIssuesValue)

    // console.log('# H2 found:'.yellow, articleH2)
    console.log('# # Tables:'.yellow, articleRealTables)
    // console.log('# # Quotes:'.yellow, articleQuote, '(UID:', articleQuoteArray+')')
    console.log('# # Quotes:'.yellow, articleQuote)
    console.log('# # Contentboxes:'.yellow, articleContentbox + ' (' + articleLink + ' with at least a link)'.yellow)
    console.log('# # Contentboxes+title:'.yellow, articleContentboxTitle)
    console.log('# # Attachments:'.yellow, articleAttachment)
    // console.log('# # Authors removed:'.yellow, authors.lenght)
  }
  objectString = JSON.stringify(object, null, 2)

  // export database
  fs.writeFile(file, objectString, (err) => {
    if (err) throw err
    console.log(colors.cyan('# Saved file:'), file)

    if (fieldsCSV) {
      let csv = json2csv({ data: object.data, flatten: true, fields: fieldsCSV, fieldNames: namesCSV })
      let jsonCSV = extReplacer(file) + '.csv'
      fs.writeFile(jsonCSV, csv, function(err) {
        if (err) throw err;
        console.log(colors.cyan('# Converting into CSV:'), jsonCSV)
      })
    }
  })
}


// Read file
const readDBFile = file => {
  fs.readFile(src + 'db/' + file, 'utf8', (err, data) => {
    if (err) throw err

    console.log(colors.cyan('# Opening file:'), file)
    console.log("# Checking all the articles elements...".cyan)

    const objRaw = replaceIssues(data)
    const obj = JSON.parse(objRaw)

    for (let article of obj.data) {
      // if (article.hidden == 1) {
      //   delete article
      //   articleHidden++
      // }

      // Add star rating keys
      for (let key of newKeys) {
        article[key] = ''
      }

      // Remove unnecessary keys
      for (let key of unusedKeys) {
        delete article[key]
      }

      // update article merging other objects
      article = matchUID(article, rating, slug_taxonomies)

      // Title
      // if ( article.title.length > 80 ) titleOver80++
      // if ( article.title.length > 90 ) titleOver90++
      // if ( article.title.length > 100 ) titleOver100++

      // Time conversion
      article.datetime = dateUnixConversion(article.datetime)
      // console.log(colors.blue('datetime: ' + article.datetime))

      // Content + attributes
      article.bodytext = removeAttributes(article.bodytext)

      // Content + images
      article.bodytext = filterImage(article.bodytext)

      // Content + PDF
      article.bodytext = filterLinks(article.bodytext)

      // Content + MS Word
      article.bodytext = removeMSWord(article.bodytext)

      // Content + Tables
      article.bodytext = tablesIssuesFinder(article)

      // Add author if empty
      // if (article.author == '') {
        article.author = 'firma.de'
      // }

      // Convert status
      if (article.hidden == 0) {
        article.hidden = 'publish'
      } else {
        article.hidden = 'private'
      }

      article.post_type = 'post'
    }

    writeDBFile(dist + file, obj, true, fieldsCSV)

    let objFiltered = filterArticles(obj)
    writeDBFile(dist + 'filtered_' + file, objFiltered, false, fieldsWP, field_namesWP)
    writeDBFile(dist + 'authors.json', authors, false, authorCSV)
    writeDBFile('/Users/cnotv/Desktop/firma-assets/src/static/' + file, objFiltered)

    if (articleImages > 0) {
      copyMedia()
    }

    resetCounters()
  })
}


// Clean folder
const cleanFolder = folder => {
  console.log('# Removed folder:'.cyan, folder)

  if (fs.existsSync(folder)) {
    if (fs.existsSync(folder+'media/')) {
      fs.removeSync(folder+'media/')
    }
    fs.removeSync(folder)
  } else {
    console.log('# There is nothing to clean'.red)
  }
}


// Read folder
const readDBFolder = folder => {
  const processStart = Date.now()

  fs.readdir(folder, (err, files) => {
    if (err) throw error
    console.log('# Reading folder:'.cyan, folder)
    console.log('Found', files.length, 'files')

    // create these objects
    console.log('# Loading database to be merged...'.cyan)
    rating = mergeRating(ratingDB)
    slug_taxonomies = csvToObj(slug_taxonomiesDB)

    if (files != 0) {
      for (const file of files) {
        let isJson = extChecker(file,'json')
        if (isJson) {


          if (!fs.existsSync(dist)) {
            fs.mkdir(dist, (err) => {
              if (err) throw err
            })
          }

          // read with merge and table conversion by default, set false for not
          readDBFile(file)
        }
      }

      const processEnd = Date.now()
      console.log('# Process completed in'.yellow, ((processEnd - processStart)/1000), 'seconds'.yellow )

    } else {
      console.log('# There are no files to inspect'.red)
    }
  })
}


cleanFolder(dist)
readDBFolder(src+'db')
